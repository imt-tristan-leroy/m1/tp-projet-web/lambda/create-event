use std::str;

use mongodb::{
    bson::{doc, oid::ObjectId},
    error::Error,
    Database,
};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssociationModel {
    pub _id: ObjectId,
    pub name: String,
    pub description: String,
}
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EventModel {
    pub _id: ObjectId,
    association_id: ObjectId,
    pub name: String,
    pub description: String,
}

pub async fn create_event(
    db: Database,
    association_id: String,
    name: String,
    description: String,
) -> Result<String, Error> {
    let collection = db.collection::<EventModel>("associations");

    let id = match ObjectId::parse_str(association_id) {
        Ok(id) => id,
        Err(err) => todo!(),
    };

    let association = get_association_by_id(db.clone(), id).await?;

    let event = EventModel {
        _id: ObjectId::new(),
        association_id: association._id,
        name,
        description,
    };

    collection.insert_one(&event, None).await?;

    Ok(event._id.to_hex())
}

pub async fn get_association_by_id(db: Database, id: ObjectId) -> Result<AssociationModel, Error> {
    let collection = db.collection::<AssociationModel>("associations");

    let filter = doc! { "_id": id };

    match collection.find_one(filter, None).await? {
        Some(association) => Ok(association),
        None => todo!(),
    }
}
